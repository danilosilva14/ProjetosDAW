<?php
$nomeArq = "teste.txt";
$arquivo = fopen("teste.txt","r");
if(!$arquivo){
    echo "Falha ao abrir arquivo ",$nomeArq,"<br>";
    exit(0);
}

echo "Arquivo  $nomeArq aberto com sucesso","<br>";

echo "Tamanho do arquivo: ", filesize($nomeArq), "<br>";

$texto = fread($arquivo, filesize($nomeArq));
echo "Conteudo do arquivo: [",$texto,"]<br>";
fclose($arquivo);

echo "<hr>";
$arquivo = file($nomeArq);
for($n=0; $n<count($arquivo); $n++){
    echo $arquivo[$n],"<br>";
}