<?php
$nomeArq = "./dados/teste2.txt";
$arquivo = fopen($nomeArq, "w");
if(!$arquivo){
    echo "Falha ao abrir o arquivo $nomeArq para gravação";
    exit(0);
}

$texto = "Conteudo acrescentando através \n da função ";
$texto .= "fwrite() do php!";
$sucesso = fwrite($arquivo, $texto);
if(!$sucesso){
    echo "Falha ao gravar no arquivo $nomeArq!<br>";
}else{
    echo "Arquivo $nomeArq atualizado com sucesso!<br>";
}